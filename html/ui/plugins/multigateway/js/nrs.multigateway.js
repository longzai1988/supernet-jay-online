/******************************************************************************
 * Copyright � 2013-2015 The Nxt Core Developers.                             *
 *                                                                            *
 * See the AUTHORS.txt, DEVELOPER-AGREEMENT.txt and LICENSE.txt files at      *
 * the top-level directory of this distribution for the individual copyright  *
 * holder information and the developer policies on copyright and licensing.  *
 *                                                                            *
 * Unless otherwise agreed in a custom licensing agreement, no part of the    *
 * Nxt software, including this file, may be copied, modified, propagated,    *
 * or distributed except according to the terms contained in the LICENSE.txt  *
 * file.                                                                      *
 *                                                                            *
 * Removal or modification of this copyright notice is prohibited.            *
 *                                                                            *
 ******************************************************************************/

/**
 * @depends {nrs.js}
 */
var NRS = (function (NRS, $, undefined) {
    var isSearchingAM = false;
    var isJay = true;
    var mgwCoinDetails = [
    { "coin": "BTC", "assetID": "17554243582654188572", "decimal": "8", "depositConfirmation": "6" }
    ];

    var mgwCoinFees = [
    { "coin": "BTC", "minDeposit": 0.0005, "minWithdraw": 0.0005, "mgwFees": 0.0001, "blockchainFees": 0.0001 }
    ];

    var mgwCoinMultigateway = [
    { "coin": "BTC", "accountRS": "NXT-3TKA-UH62-478B-DQU6K" }
    ];

    var mgwCoinMgwServer = [
    { "coin": "BTC", "server": ["8593269027165738667", "2986384496629142530", "2406158154854548637", "12736719038753962716"] }
    ];

    var mgwCoinDepositJson = [
    { "coin": "BTC", "jsonAM": '{"BTC":""}', "gateway": "94c8f273" }
    ];

    var mgwCoinDepositAddr = [
    { "coin": "BTC", "depositAddr": "" }
    ];
    var mgwCoin = "";
    var mgwCoinAssetID = "";
    var mgwCoinDecimal = 0;
    var mgwCoinBalance = 0;
    var mgwCoinUnconfirmedBalance = 0;
    var tour = null;
    var path = "../plugins/multigateway/";
    var timer;

    NRS.pages.multigateway = function () {
        changeCoin("BTC");
        refreshMGW(false);
        NRS.pageLoaded();
    }
    NRS.setup.multigateway = function () {
        //Do one-time initialization stuff here
        $("#mgw_coinlist").select2({
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) { return m; }
        });

        initAfterLogin();

        if (NRS.rememberPassword) $(".secret_phrase").hide();
    }

    NRS.mgwShowDemo = function () {
        initTour();
        tour.restart();
    }
    NRS.mgwGenerateOnClick = function () {
        $("#mgw_gen_deposit_form .error_message").html("").hide();
        $("#mgw_gen_deposit_addr_password").val('');
        var result = $.grep(mgwCoinMultigateway, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });
        $("#mgw_gen_deposit_addr_recipient").val(result[0].accountRS);

        var msg = getGenDepositBytes();
        $("#mgw_gen_deposit_addr_msg").val(msg);

        $('#mgw_gen_deposit_addr_modal').modal('show');
        $("#mgw_gen_deposit_addr_modal .jay-tx").hide();
    }
    NRS.mgwWithdrawCoin = function(){
        var result = $.grep(mgwCoinFees, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });

        var minWithdrawNQT = new Big(NRS.convertToNQT(result[0].minWithdraw).toString());
        var withdrawAmountNQT = new Big(NRS.convertToNQT($("#mgw_coin_withdraw_amount").val()).toString());
        var mgwCoinUnconfirmedBalanceNQT = new Big(NRS.convertToNQT(mgwCoinUnconfirmedBalance).toString());

        if ($.trim($("#mgw_coin_withdraw_addr").val()) == "") {
            $.growl("Please specify your " + mgwCoin + " address", {
                "type": "danger"
            });
        }
        else if (withdrawAmountNQT.cmp(minWithdrawNQT) == -1) {
            $.growl("Minimum withdraw is " + result[0].minWithdraw + " " + mgwCoin + ".", {
                "type": "danger"
            });
        }
        else if (mgwCoinUnconfirmedBalanceNQT.cmp(withdrawAmountNQT) == -1) {
            $.growl("You have insufficient " + mgwCoin + " to withdraw", {
                "type": "danger"
            });
        }
        else {
            if (NRS.accountInfo.unconfirmedBalanceNQT >= 100000000) {
                var resultMGW = $.grep(mgwCoinMultigateway, function (_mgwCoin2) { return _mgwCoin2.coin == mgwCoin });
                var resultCoinDetails = $.grep(mgwCoinDetails, function (_mgwCoin3) { return _mgwCoin3.coin == mgwCoin });

                $("#mgw_withdraw_modal_address").text($.trim($("#mgw_coin_withdraw_addr").val()));
                $("#mgw_withdraw_modal_total_received").text($("#mgw_coin_mgw_total_received").val() + " " + mgwCoin);
                $("#mgw_withdraw_modal_message").val('{"redeem":"' + mgwCoin + '","withdrawaddr":"' + $.trim($("#mgw_coin_withdraw_addr").val()) + '","InstantDEX":""}');
                $("#mgw_withdraw_modal_comment").val('{"redeem":"' + mgwCoin + '","withdrawaddr":"' + $.trim($("#mgw_coin_withdraw_addr").val()) + '","InstantDEX":""}');
                $("#mgw_withdraw_modal_quantityQNT").val(NRS.convertToQNT($("#mgw_coin_withdraw_amount").val(), mgwCoinDecimal));
                $("#mgw_withdraw_modal_feeNXT").val(1);
                $("#mgw_withdraw_modal_deadline").val(24);
                $("#mgw_withdraw_modal_add_message").prop('checked', true);

                $("#mgw_withdraw_modal_recipient").val(resultMGW[0].accountRS);
                $("#mgw_withdraw_modal_asset").val(resultCoinDetails[0].assetID);

                $('#mgw_withdraw_modal').modal('show');
                $("#mgw_withdraw_modal .jay-tx").hide();
            }
            else {
                $.growl("Withdraw " + mgwCoin + " required 1 NXT", {
                    "type": "danger"
                });
            }
        }
    }
    NRS.mgwSendMessage = function () {
        $("#mgw_gen_deposit_addr_modal .btn-primary").button('loading');
        var data = {
            "messageIsText": false,
            "message": $.trim($("#mgw_gen_deposit_addr_msg").val()),
            "recipient": $.trim($("#mgw_gen_deposit_addr_recipient").val()),
            "feeNXT": $.trim($("#mgw_gen_deposit_addr_fee").val()),
            "deadline": $.trim($("#mgw_gen_deposit_addr_deadline").val()),
            "secretPhrase": $.trim($("#mgw_gen_deposit_addr_password").val())
        };

        if (isJay) {
            NRS.getJayCode("sendMessage", data, $("#mgw_gen_deposit_addr_modal"));
            $("#mgw_gen_deposit_addr_modal .btn-primary").button('reset');
        } else {
            NRS.sendRequest("sendMessage", data, function (response) {
                $("#mgw_gen_deposit_addr_modal .btn-primary").button('reset');
                if (response.errorDescription) {
                    $("#mgw_gen_deposit_form .error_message").html(response.errorDescription).show();
                }
                else {
                    $("#mgw_gen_deposit_form .error_message").html("").hide();
                    $('#mgw_gen_deposit_addr_modal').modal('hide');
                    $.growl("Your deposit address will be generated in 15 blocks.", {
                        "type": "success"
                    });
                }
            });
        }
    }
    NRS.mgwTransferAsset = function () {
        modal = $("#mgw_withdraw_modal");
        $("#mgw_withdraw_modal .btn-primary").button('loading');
        var data = NRS.getFormData(modal.find("form:first"));

        if (isJay) {
            NRS.getJayCode("transferAsset", data, $("#mgw_withdraw_modal"));
            $("#mgw_withdraw_modal .btn-primary").button('reset');
        } else {
            NRS.sendRequest("transferAsset", data, function (response) {
                $("#mgw_withdraw_modal .btn-primary").button('reset');
                if (response.errorDescription) {
                    $("#mgw_withdraw_modal .error_message").html(response.errorDescription).show();
                }
                else {
                    $("#mgw_withdraw_modal .error_message").html("").hide();
                    $('#mgw_withdraw_modal').modal('hide');
                    $.growl("Your withdrawal has been submitted.", {
                        "type": "success"
                    });

                    setTimeout(function () {
                        refreshAsset();
                    }, 5000);
                }
            });
        }
    }
    NRS.mgwWithdrawAmountKeyDown = function (e) {
        var resultCoinDetails = $.grep(mgwCoinDetails, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });

        var charCode = !e.charCode ? e.which : e.charCode;
        if (isControlKey(charCode) || e.ctrlKey || e.metaKey) {
            return;
        }

        var maxFractionLength = resultCoinDetails[0].decimal;

        if (maxFractionLength) {
            //allow 1 single period character
            if (charCode == 110 || charCode == 190) {
                if ($("#mgw_coin_withdraw_amount").val().indexOf(".") != -1) {
                    e.preventDefault();
                    return false;
                } else {
                    return;
                }
            }
        } else {
            //do not allow period
            if (charCode == 110 || charCode == 190 || charCode == 188) {
                $.growl("Fractions are not allowed.", {
                    "type": "danger"
                });
                e.preventDefault();
                return false;
            }
        }

        var input = $("#mgw_coin_withdraw_amount").val() + String.fromCharCode(charCode);
        var afterComma = input.match(/\.(\d*)$/);

        //only allow as many as there are decimals allowed..
        if (afterComma && afterComma[1].length > maxFractionLength) {
            var errorMessage = "Only " + resultCoinDetails[0].decimal + " digits after the decimal mark are allowed for this asset.";
            $.growl(errorMessage, {
                "type": "danger"
            });

            e.preventDefault();
            return false;
        }

        //numeric characters, left/right key, backspace, delete, home, end
        if (charCode == 8 || charCode == 37 || charCode == 39 || charCode == 46 || charCode == 36 || charCode == 35 || (charCode >= 48 && charCode <= 57 && !isNaN(String.fromCharCode(charCode))) || (charCode >= 96 && charCode <= 105)) {
        } else {
            //comma
            if (charCode == 188) {
                $.growl("Comma is not allowed, use a dot instead.", {
                    "type": "danger"
                });
            }
            e.preventDefault();
            return false;
        }
    }
    NRS.mgwWithdrawAmountKeyUp = function (e) {
        var result = $.grep(mgwCoinFees, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });

        var blockchainFeesNQT = new Big(NRS.convertToNQT(result[0].blockchainFees));
        var mgwFeesNQT = new Big(NRS.convertToNQT(result[0].mgwFees));

        var minFeesNQT = blockchainFeesNQT.plus(mgwFeesNQT);
        var withdrawAmountNQT = new Big(NRS.convertToNQT($("#mgw_coin_withdraw_amount").val()));

        if (withdrawAmountNQT.cmp(minFeesNQT) == 1) {
            var total = withdrawAmountNQT.minus(minFeesNQT);
            $("#mgw_coin_mgw_total_received").val(NRS.convertToNXT(total.toString()));
        }
        else {
            $("#mgw_coin_mgw_total_received").val('0');
        }
    }

    function initAfterLogin() {
        changeCoin("BTC");
        refreshMGW(true);

        if (timer) {
            clearInterval(timer);
        }

        timer = setInterval(function () {
            refreshMGW(NRS.lastBlockHeight % 10 == 0)
        }, 60000);
    }
    function initTour() {
        tour = new Tour({
            steps: [
            {
                orphan: true,
                backdrop: true,
                title: "Welcome to Multigateway Tour",
                content: "Click <a href='http://nxtra.org/faucet/' target='_blank'>here</a> to start your tour with some free NXT.<br/><br/>Your account ID is: <br/><b>" + NRS.accountRS + "</b>"
            },
            {
                element: "#mgw_coinlist_div",
                placement: 'left',
                title: "Supported Coin List",
                content: "Click here for a list of supported coins in Multigateway."
            },
            {
                element: "#mgw_deposit_top_label",
                placement: 'right',
                title: "Deposit Address",
                content: "When the searching is completed, generate your MGW converting account to deposit your preferred coin and start trading in asset exchange."
            },
            {
                element: "#mgw_balance_div",
                placement: 'right',
                title: "Asset Balance",
                content: "Trade with your balance shown here."
            }
            ,
            {
                element: "#sidebar_asset_exchange",
                placement: 'top',
                title: "Asset Exchange",
                content: "Experience your first trading in asset exchange."
            },
            {
                element: "#mgw_withdraw_top_label",
                placement: 'left',
                title: "Withdraw Account Balance",
                content: "Send your MGW account balance to your personal coin wallet."
            }
            ]
        });

        tour.init();
    }
    function initCoinDetails() {
        var result = $.grep(mgwCoinDetails, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });
        mgwCoinAssetID = result[0].assetID;
        mgwCoinDecimal = parseInt(result[0].decimal);

        $("#mgw_coin_asset_id").text(result[0].assetID);
        $("#mgw_coin_confirmation").text(result[0].depositConfirmation);

        $("#mgw_coin_tab").html("<img src='" + path + "img/" + mgwCoin + ".png'/> " + mgwCoin);
        $("#mgw_coin_asset_id_label").text("mgw" + mgwCoin + " Asset ID");
        $("#mgw_coin_withdraw_addr").attr("placeholder", mgwCoin + " Wallet Address");
        $("#mgw_coin_withdraw_confirmation").attr("src", path + "img/" + mgwCoin + ".png");

        for (var x = 1; x <= 22; x++) {
            $("#mgw_coin" + x).text(mgwCoin);
        }
    }
    function initCoinFees() {
        var result = $.grep(mgwCoinFees, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });
        $("#mgw_coin_blockchain_fee").attr("placeholder", result[0].blockchainFees);
        $("#mgw_coin_mgw_fee").attr("placeholder", result[0].mgwFees);
        $("#mgw_coin_min_withdraw").text(result[0].minWithdraw);

        for (var x = 1; x <= 2; x++) {
            $("#mgw_coin_min_deposit" + x).text(result[0].minDeposit);
        }
    }
    function initCoinAssetBalance() {
        mgwCoinBalance = 0;
        mgwCoinUnconfirmedBalance = 0;
        try {
            for (var i = 0; i < NRS.accountInfo.assetBalances.length; i++) {
                if (NRS.accountInfo.assetBalances[i].asset == mgwCoinAssetID) {
                    mgwCoinBalance = NRS.convertToNXT(new BigInteger(NRS.accountInfo.assetBalances[i].balanceQNT.toString()).multiply(new BigInteger(Math.pow(10, 8 - mgwCoinDecimal).toString())));
                }
            }

            for (var i = 0; i < NRS.accountInfo.unconfirmedAssetBalances.length; i++) {
                var balance = NRS.accountInfo.unconfirmedAssetBalances[i];

                if (balance.asset == mgwCoinAssetID) {
                    mgwCoinUnconfirmedBalance = NRS.convertToNXT(new BigInteger(balance.unconfirmedBalanceQNT.toString()).multiply(new BigInteger(Math.pow(10, 8 - mgwCoinDecimal).toString())));
                }
            }
            $("#mgw_coin_balance").text(mgwCoinBalance);
            $("#mgw_coin_unconfirmed_balance").text(mgwCoinUnconfirmedBalance);

            var array = mgwCoinUnconfirmedBalance.toString().split('.');
            if (array.length > 1) {
                $("#mgw_coin_asset_balance").text(array[0]);
                $("#mgw_coin_asset_balance_decimal").text("." + array[1]);
            }
            else {
                $("#mgw_coin_asset_balance").text(array[0]);
                $("#mgw_coin_asset_balance_decimal").text('');
            }
        }
        catch (ex) {
            mgwCoinBalance = 0;
            mgwCoinUnconfirmedBalance = 0;
            $("#mgw_coin_asset_balance").text('0');
            $("#mgw_coin_asset_balance_decimal").text('');
            $("#mgw_coin_balance").text('0');
            $("#mgw_coin_unconfirmed_balance").text('0');
        }
    }
    //function initDepositAddr(bIsStartSearch) {
    //    var result = $.grep(mgwCoinDepositAddr, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });
    //    var resultTXID = [];

    //    if (!isSearchingAM) {
    //        if (bIsStartSearch) {
    //            isSearchingAM = true;
    //            if (result[0].depositAddr == "") {
    //                $("#mgw_coin_gendeposit_loading_div").show();
    //                $("#mgw_coin_gendeposit_div").hide();
    //                $("#mgw_coin_deposit_addr_div").hide();
    //            }

    //            var mgwAccount = getMGWdistinctAccount();

    //            $.each(mgwAccount, function (mgwAccountIndex, mgwAccountValue) {
    //                $.ajax({
    //                    url: '/nxt?requestType=getAccountTransactionIds&account=' + mgwAccountValue + '&timestamp=0&type=1&subtype=0',
    //                    dataType: 'json',
    //                    async: false,
    //                    success: function (result) {
    //                        $.each(result["transactionIds"], function (txidIndex, txidValue) {
    //                            resultTXID.push(txidValue);
    //                        });
    //                    }
    //                });
    //            });

    //            loopAM(resultTXID, 0);
    //        }
    //        else {
    //            initDepositAddrDone();
    //        }
    //    }
    //}
    function getDepositAddr(bIsStartSearch) {
        if (bIsStartSearch) {
            $("#mgw_coin_gendeposit_loading_div").show();
            $("#mgw_coin_gendeposit_div").hide();
            $("#mgw_coin_deposit_addr_div").hide();

            var mgwAccount = getMGWdistinctAccount();

            $.each(mgwAccount, function (mgwAccountIndex, mgwAccountValue) {
                $.ajax({
                    url: '/nxt?requestType=getBlockchainTransactions&account=' + mgwAccountValue + '&timestamp=0&type=1&subtype=0&withMessage=true',
                    dataType: 'json',
                    async: false,
                    success: function (result) {
                        $.each(result["transactions"], function (txidIndex, txidValue) {
                            try{
                                var strHex = txidValue.attachment.message;
                                strHex = strHex.toString().substring(64);
                                var strMsg = converters.hexStringToString(strHex);

                                var jsonMsg = $.parseJSON(strMsg);
                                if (jsonMsg.NXTaddr == NRS.account) {
                                    var result = $.grep(mgwCoinDepositAddr, function (_mgwCoin) { return _mgwCoin.coin == jsonMsg.coin });
                                    if (result.length == 1) {
                                        var resultServer = $.grep(mgwCoinMgwServer, function (_mgwCoinServer) { return _mgwCoinServer.coin == jsonMsg.coin });
                                        for (var x = 0; x < resultServer[0].server.length; x++) {
                                            if (txidValue.sender == resultServer[0].server[x]) {
                                                if (!result[0].depositAddr) {
                                                    if (isJay) {
                                                        $("#faMgwVerifyDeposit").data("transaction", txidValue.transaction).show(); 
                                                    }
                                                    result[0].depositAddr = jsonMsg.address;
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (err) {

                            }
                        });

                        initDepositAddrDone();
                    }
                });
            });
        } else {
            initDepositAddrDone();
        }
    }
    function getMGWdistinctAccount() {
        var result = [];

        $.each(mgwCoinMultigateway, function (i, v) {
            if ($.inArray(v["accountRS"], result) == -1) result.push(v["accountRS"]);
        });
        return result;
    }
    function initDepositAddrDone() {
        var result = $.grep(mgwCoinDepositAddr, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });

        if (result[0].depositAddr != "") {
            $("#mgw_coin_gendeposit_loading_div").hide();
            $("#mgw_coin_gendeposit_div").hide();
            $("#mgw_coin_deposit_addr_div").show();

            $("#mgw_coin_deposit_addr").text(result[0].depositAddr);
        }
        else {
            $("#mgw_coin_gendeposit_loading_div").hide();
            $("#mgw_coin_gendeposit_div").show();
            $("#mgw_coin_deposit_addr_div").hide();

            $("#mgw_coin_deposit_addr").text("");
        }

        $("#mgw_coin_deposit_pct").css('width', 0 + '%');
        $("#mgw_coin_deposit_pct").html(0 + '%');
    }
    function refreshMGW(bIsStartSearch) {
        refreshAsset();
        getDepositAddr(bIsStartSearch);
    }
    function refreshAsset() {
        initializeShowLoading();

        setTimeout(function () {
            initCoinAssetBalance();
            initializeHideLoading();
        }, 1500);
    }
    function initializeShowLoading() {
        $("#mgw_coin_asset_balance_loading").show();
        $("#mgw_coin_asset_balance").hide();
        $("#mgw_coin_asset_balance_decimal").hide();
    }
    function initializeHideLoading() {
        $("#mgw_coin_asset_balance_loading").hide();
        $("#mgw_coin_asset_balance").show();
        $("#mgw_coin_asset_balance_decimal").show();
    }
    function loopAM(array, count) {
        bCont = true;
        var pct = Math.round((count / array.length) * 100);

        $("#mgw_coin_deposit_pct").css('width', pct + '%');
        $("#mgw_coin_deposit_pct").html(pct + '%');

        try {
            $.ajax({
                url: '/nxt?requestType=getTransaction&transaction=' + array[count],
                dataType: 'json',
                async: true,
                success: function (data) {
                    try {
                        if (count < array.length) {
                            var strHex = data.attachment.message;
                            strHex = strHex.toString().substring(64);
                            var strMsg = converters.hexStringToString(strHex);

                            var jsonMsg = $.parseJSON(strMsg);

                            if (jsonMsg.NXTaddr == NRS.account) {
                                var result = $.grep(mgwCoinDepositAddr, function (_mgwCoin) { return _mgwCoin.coin == jsonMsg.coin });
                                if (result.length == 1) {
                                    var resultServer = $.grep(mgwCoinMgwServer, function (_mgwCoinServer) { return _mgwCoinServer.coin == jsonMsg.coin });

                                    for (var x = 0; x < resultServer[0].server.length; x++) {
                                        if (data.sender == resultServer[0].server[x]) {
                                            result[0].depositAddr = jsonMsg.address;
                                        }
                                        else {
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            bCont = false;
                            initDepositAddrDone();
                        }

                        if (bCont) {
                            loopAM(array, count + 1);
                        }
                        else {
                            isSearchingAM = false;
                        }
                    }
                    catch (ex) {
                        loopAM(array, count + 1);
                    }
                }
            });
        } catch (ex) {

        }
    }
    function changeCoin(coin) {
        mgwCoin = coin;
        initCoinDetails();
        initCoinFees();
        clearField();
    }
    function getGenDepositBytes() {
        var result = $.grep(mgwCoinDepositJson, function (_mgwCoin) { return _mgwCoin.coin == mgwCoin });

        var sig = result[0].gateway;
        var size = "";
        var nxt64bits = nsv_mgw_hexConverter.decToHex(NRS.account);
        var funcid = "67000000";
        var gatewayid = "00000000";
        var timestamp = "00000000";
        var jsonflag = "01000000";
        var json_AM = result[0].jsonAM;
        var json_AM_hex = converters.stringToHexString(json_AM);

        nxt64bits = nsv_mgw_padZero(nxt64bits, 16);
        nxt64bits = nsv_mgw_reverseHex(nxt64bits);

        size = 48 + (json_AM_hex.toString().length / 2) + 1;
        size = nsv_mgw_hexConverter.decToHex(size.toString());
        size = nsv_mgw_padZero(size, 8);
        size = nsv_mgw_reverseHex(size);

        var msg = sig + size + nxt64bits + funcid + gatewayid + timestamp + jsonflag + json_AM_hex;

        return msg;
    }
    function clearField() {
        $("#mgw_coin_withdraw_addr").val("");
        $("#mgw_coin_withdraw_amount").val("");
        $("#mgw_coin_mgw_total_received").val("0");
    }
    function format(coin) {
        if (!coin.id) return coin.text;
        return "<img src='" + path + "img/" + coin.id + ".png'/> " + coin.text;
    }
    function isControlKey(charCode) {
        if (charCode >= 32)
            return false;
        if (charCode == 10)
            return false;
        if (charCode == 13)
            return false;
        return true;
    }

    $("#mgw_coinlist").on("change", function (e) {
        changeCoin(e.val);
        refreshMGW(false);
    });
    $('#mgw_gen_deposit_addr_modal').on('shown.bs.modal', function () {
        $('#mgw_gen_deposit_addr_password').focus();
    })
    $('#mgw_withdraw_modal').on('hidden.bs.modal', function () {
        setTimeout(function () {
            refreshAsset();
        }, 5000);

        $("#mgw_coin_withdraw_amount").val("");
        $("#mgw_coin_mgw_total_received").val("0");
    })
    return NRS;
}(NRS || {}, jQuery));